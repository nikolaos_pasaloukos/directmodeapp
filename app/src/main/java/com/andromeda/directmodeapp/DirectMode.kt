package com.andromeda.directmodeapp

import android.content.*
import android.os.*
import android.support.v7.app.AppCompatActivity
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*

import kotlinx.android.synthetic.main.activity_direct_mode.*

class DirectMode : AppCompatActivity() {
    companion object {
        fun useService(): Boolean {
            return true
        }
    }

    private val TAG = "DirectModeApp.DirectMoode"
    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v4.app.FragmentStatePagerAdapter].
     */
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private val mainFragment: MainFragment = MainFragment()
    private val recordingFragment: RecordingFragment = RecordingFragment()

    /**
     * Class for interacting with the main interface of the service.
     */
    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            mainFragment.getDMAPI().mService = Messenger(service)
            DirectModeAPI.mBound = true
            mainFragment.getDMAPI().registerListener()
            mainFragment.getDMAPI().sendMessageInt(DirectModeAPI.MSG_POWER_DIRECT_MODE, -1)
            mainFragment.getDMAPI().sendMessageInt(DirectModeAPI.MSG_SELECT_VOLUME, -1)
            mainFragment.getDMAPI().sendMessageInt(DirectModeAPI.MSG_SELECT_CHANNEL, -1)
        }

        override fun onServiceDisconnected(className: ComponentName) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mainFragment.getDMAPI().mService = null
            DirectModeAPI.mBound = false
            mainFragment.getDMAPI().unregisterListener()
        }
    }

    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            Log.d(TAG, intent.toString())
            val pttButton = findViewById<Button>(R.id.ptt_button)
            if (pttButton?.isEnabled == false)
                return
            when (intent?.action) {
                getString(R.string.GENAKER_PTT_DOWN),
                getString(R.string.MOTOROLA_PTT_DOWN),
                getString(R.string.ZELLO_PTT_DOWN),
                getString(R.string.ANDROMEDA_PTT_DOWN) -> {
                    if (!useService())
                        mainFragment.getDMAPI().beginTransmit()
                }
                getString(R.string.GENAKER_PTT_UP),
                getString(R.string.MOTOROLA_PTT_UP),
                getString(R.string.ZELLO_PTT_UP),
                getString(R.string.ANDROMEDA_PTT_UP) -> {
                    if (!useService())
                        mainFragment.getDMAPI().endTransmit()
                }
            }
        }
    }

    fun onCheckboxClick(v: View) {
        val micPCM = findViewById<CheckBox>(R.id.pcm_mic)
        val audioPCM = findViewById<CheckBox>(R.id.pcm_audio)
        val sendId = findViewById<CheckBox>(R.id.sendID)
        if (v == micPCM) {
            DirectModeAPI.micThroughPCM = micPCM.isChecked
            DirectModeKeepAlive.micPCM = micPCM.isChecked
            mainFragment.getDMAPI().sendMessageInt(DirectModeAPI.MSG_MIC_TO_DIRECTMODE, when (micPCM.isChecked) {true -> 0; false -> 1})
        } else if (v == audioPCM) {
            DirectModeAPI.audioThroughPCM = audioPCM.isChecked
            DirectModeKeepAlive.audioPCM = audioPCM.isChecked
            mainFragment.getDMAPI().sendMessageInt(DirectModeAPI.MSG_AUDIO_TO_SPEAKER, when (audioPCM.isChecked) {true -> 0; false -> 1})
        } else if (v == sendId) {
            DirectModeAPI.sendID = sendId.isChecked
            DirectModeKeepAlive.sendID = sendId.isChecked
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_direct_mode)

        setSupportActionBar(toolbar)
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        container.adapter = mSectionsPagerAdapter
    }

    override fun onStart() {
        Log.d(TAG, "onStart")
        super.onStart()
        // Bind to the service
        if (!useService())
            bindService(DirectModeAPI.getServiceIntent(), mConnection, Context.BIND_AUTO_CREATE)
        else {
            if (!DirectModeKeepAlive.running) {
                val intent = Intent(this, DirectModeKeepAlive::class.java)
                DirectModeKeepAlive.running = true
                startForegroundService(intent)
            }
            val dmIFilter = IntentFilter()
            //adding some filters
            dmIFilter.addAction(DirectModeAPI.INTENT_SET_OWN_ID)
            dmIFilter.addAction(DirectModeAPI.INTENT_TRANSMIT)
            dmIFilter.addAction(DirectModeAPI.INTENT_DIRECT_MODE)
            dmIFilter.addAction(DirectModeAPI.INTENT_CHANNEL)
            dmIFilter.addAction(DirectModeAPI.INTENT_TEXT_MESSAGE)
            dmIFilter.addAction(DirectModeAPI.INTENT_VOLUME)
            dmIFilter.addAction(DirectModeAPI.INTENT_POWER)
            dmIFilter.addAction(DirectModeAPI.INTENT_CHIP_UPDATE)
            dmIFilter.addAction(DirectModeAPI.INTENT_CHIP_LOCK_UP)
            dmIFilter.addAction(DirectModeAPI.INTENT_REGISTER_LISTENER)
            LocalBroadcastManager.getInstance(this).registerReceiver(mainFragment.getDMAPI().serviceIntentReceiver, dmIFilter)
        }
        // Start listening for the PTT button
        val iFilter = IntentFilter()
        iFilter.addAction(getString(R.string.GENAKER_PTT_DOWN))
        iFilter.addAction(getString(R.string.GENAKER_PTT_UP))
        iFilter.addAction(getString(R.string.MOTOROLA_PTT_DOWN))
        iFilter.addAction(getString(R.string.MOTOROLA_PTT_UP))
        iFilter.addAction(getString(R.string.ZELLO_PTT_DOWN))
        iFilter.addAction(getString(R.string.ZELLO_PTT_UP))
        iFilter.addAction(getString(R.string.ANDROMEDA_PTT_DOWN))
        iFilter.addAction(getString(R.string.ANDROMEDA_PTT_UP))
        LocalBroadcastManager.getInstance(this).registerReceiver(broadCastReceiver, iFilter)
    }

    private fun unbind() {
        if (DirectModeAPI.mBound) {
            unbindService(mConnection)
            DirectModeAPI.mBound = false
        }
        if (useService())
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mainFragment.getDMAPI().serviceIntentReceiver)
        else
            mainFragment.getDMAPI().unregisterListener()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadCastReceiver)
    }

    override fun onStop() {
        Log.d(TAG, "onStop")
        unbind()
        super.onStop()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_direct_mode, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_settings) {
            val intent = Intent(baseContext, DirectModeKeepAlive::class.java)
            DirectModeKeepAlive.running = false
            stopService(intent)
            onBackPressed()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        Log.d(TAG, "onBackPressed")
        unbind()
        super.onBackPressed()
    }

    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? {
            when (position) {
                0 -> return mainFragment
                1 -> return recordingFragment
            }
            return null
        }

        override fun getCount(): Int {
            // Show 2 total pages.
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence? {
            when (position) {
                0 -> return "DirectMode PTT"
                1 -> return "DirectMode Recording"
            }
            return null
        }
    }
}
