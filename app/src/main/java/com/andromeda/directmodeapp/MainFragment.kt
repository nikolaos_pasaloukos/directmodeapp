package com.andromeda.directmodeapp

import android.os.*
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import android.widget.*

class MainFragment : Fragment(),
    CompoundButton.OnCheckedChangeListener,
    View.OnTouchListener,
    NumberPicker.OnValueChangeListener,
    View.OnKeyListener  {
    companion object {
        var directModeAPI: DirectModeAPI? = null
    }

    private val TAG = "DirectModeApp.MainFragment"
    /** Flag indicating whether we have set our Own ID or not.  */
    private var mOwnIdSet: Boolean = false

    var rootView: View? = null

    private val dmAPI: DirectModeAPI = DirectModeAPI(this)

    fun getDMAPI(): DirectModeAPI {
        return dmAPI
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        rootView = inflater.inflate(R.layout.fragment_main, container, false)
        directModeAPI = dmAPI
        val enableSwitch = rootView?.findViewById<Switch>(R.id.enable_switch)
        enableSwitch?.setOnCheckedChangeListener(this)
        val pttButton = rootView?.findViewById<Button>(R.id.ptt_button)
        pttButton?.setOnTouchListener(this)
        pttButton?.isEnabled = false
        val idButton = rootView?.findViewById<Button>(R.id.id_btn)
        idButton?.setOnTouchListener(this)
        val msgButton = rootView?.findViewById<Button>(R.id.msg_btn)
        msgButton?.setOnTouchListener(this)
        msgButton?.isEnabled = false
        val txEditor = rootView?.findViewById<EditText>(R.id.txText)
        txEditor?.setOnKeyListener(this)
        val channelPicker = rootView?.findViewById<NumberPicker>(R.id.channel_picker)
        channelPicker?.minValue = 1
        channelPicker?.maxValue = 16
        channelPicker?.setOnValueChangedListener(this)
        channelPicker?.isEnabled = false
        val volumePicker = rootView?.findViewById<NumberPicker>(R.id.volume_picker)
        volumePicker?.minValue = 1
        volumePicker?.maxValue = 11
        volumePicker?.setOnValueChangedListener(this)
        volumePicker?.isEnabled = true
        if (DirectModeKeepAlive.running) {
            enableSwitch?.isChecked = DirectModeKeepAlive.enabled
            pttButton?.isEnabled = DirectModeKeepAlive.enabled
            msgButton?.isEnabled = DirectModeKeepAlive.enabled
            channelPicker?.isEnabled = DirectModeKeepAlive.enabled
            volumePicker?.value = DirectModeKeepAlive.volume
            channelPicker?.value = DirectModeKeepAlive.channel
        }
        return rootView
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        val pttButton = rootView?.findViewById<Button>(R.id.ptt_button)
        val msgButton = rootView?.findViewById<Button>(R.id.msg_btn)
        val channelPicker = rootView?.findViewById<NumberPicker>(R.id.channel_picker)
        if (isChecked) {
            dmAPI.enableDirectMode()
        } else {
            pttButton?.isEnabled = false
            msgButton?.isEnabled = false
            channelPicker?.isEnabled = false
            dmAPI.disableDirectMode()
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        if (event == null || v == null) {
            return false
        }
        v.performClick()
        val pttButton = view?.findViewById<Button>(R.id.ptt_button)
        val idButton = view?.findViewById<Button>(R.id.id_btn)
        val msgButton = view?.findViewById<Button>(R.id.msg_btn)
        if (v == pttButton) {
            if (event.action == MotionEvent.ACTION_DOWN && v.isEnabled) {
                dmAPI.beginTransmit()
            } else if (event.action == MotionEvent.ACTION_UP && v.isEnabled) {
                dmAPI.endTransmit()
            }
        } else if (v == idButton) {
            val txEditor = view?.findViewById<EditText>(R.id.txText)
            if (event.action == MotionEvent.ACTION_DOWN && v.isEnabled) {
                val id = txEditor?.text.toString()
                Log.d(TAG, "Set Own ID: $id")
                dmAPI.sendMessageStr(DirectModeAPI.MSG_SET_OWN_ID, id)
                mOwnIdSet = true
            } else if (event.action == MotionEvent.ACTION_UP && v.isEnabled)
                txEditor?.text?.clear()
        } else if (v == msgButton) {
            val txEditor = view?.findViewById<EditText>(R.id.txText)
            if (event.action == MotionEvent.ACTION_DOWN && v.isEnabled) {
                val msg = txEditor?.text.toString()
                Log.d(TAG, "Send Message: $msg")
                dmAPI.sendMessageStr(DirectModeAPI.MSG_TEXT_MESSAGE, msg)
            } else if (event.action == MotionEvent.ACTION_UP && v.isEnabled)
                txEditor?.text?.clear()
        }
        return true
    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if (!mOwnIdSet || keyCode != KeyEvent.KEYCODE_ENTER)
            return false
        val txEditor = view?.findViewById<EditText>(R.id.txText)
        if (event?.action == KeyEvent.ACTION_DOWN && v?.isEnabled == true) {
            val msg = txEditor?.text.toString()
            Log.d(TAG, "Send Message: $msg")
            dmAPI.sendMessageStr(DirectModeAPI.MSG_TEXT_MESSAGE, msg)
        } else if ((event?.action == KeyEvent.ACTION_UP) && (v?.isEnabled == true)) {
            txEditor?.text?.clear()
        }
        return true
    }

    override fun onValueChange(picker: NumberPicker?, oldVal: Int, newVal: Int) {
        if (picker == view?.findViewById<NumberPicker>(R.id.channel_picker))
            dmAPI.selectChannel(newVal)
        else
            dmAPI.selectVolume(newVal)
    }
}