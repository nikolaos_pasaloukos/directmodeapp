package com.andromeda.directmodeapp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager

class MyReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }
}
