package com.andromeda.directmodeapp

import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.media.*
import android.os.*
import android.util.Log
import android.widget.*
import java.io.IOException
import java.lang.Exception

class DirectModeAPI(mainFragment: MainFragment?) {
    companion object {
        const val MSG_POWER_DIRECT_MODE = 1
        const val MSG_SELECT_CHANNEL = 3
        const val MSG_BEGIN_TRANSMIT = 4
        const val MSG_END_TRANSMIT = 5
        const val MSG_SELECT_VOLUME = 6
        const val MSG_REGISTER_LISTENER = 7
        const val MSG_TEXT_MESSAGE = 8
        const val MSG_SET_OWN_ID = 9
        const val MSG_UPDATE_NOTIFY = 10
        const val MSG_MIC_TO_DIRECTMODE = 11
        const val MSG_AUDIO_TO_SPEAKER = 12
        const val MSG_DIRECT_MODE_LOCK_UP = 13

        const val INTENT_DIRECT_MODE = "andromeda.directmode.start"
        const val INTENT_CHANNEL = "andromeda.directmode.parameters"
        const val INTENT_POWER = "andromeda.directmode.power"
        const val INTENT_TRANSMIT = "andromeda.directmode.ptt"
        const val INTENT_VOLUME = "andromeda.directmode.volume"
        const val INTENT_TEXT_MESSAGE = "andromeda.directmode.message"
        const val INTENT_SET_OWN_ID = "andromeda.directmode.device_id"
        const val INTENT_CHIP_UPDATE = "andromeda.directmode.chip_update"
        const val INTENT_CHIP_LOCK_UP = "andromeda.directmode.chip_lock_up"
        const val INTENT_REGISTER_LISTENER = "andromeda.directmode.register"

        fun getServiceIntent(): Intent {
            val intent = Intent("com.andromeda.directmode.DIRECT_MODE_SERVICE")
            val componentName = ComponentName("com.andromedadigital.andromedadirectmode",
                "com.andromedadigital.andromedadirectmode.DirectModeService")
            intent.component = componentName
            return intent
        }

        /** Flag indicating whether we have called bind on the service.  */
        var mBound: Boolean = false
        /** Flag indicating whether the microphone will be send through a stream */
        var micThroughPCM = false
        /** Flag indicating whether DirectMode audio will be send through a stream */
        var audioThroughPCM = false
        /** Flag indicating whether to send our Own ID before a transmission */
        var sendID = true

        val fileName: String = "/last_transmission.aac"
    }

    private val TAG = "DirectModeApp.DirectMoodeAPI"
    private val mf = mainFragment
    private val useService = DirectMode.useService()

    private var recorder: MediaRecorder? = null
    private var player: MediaPlayer? = null

    /** Messenger for communicating with the service.  */
    var mService: Messenger? = null

    var serviceContext: Context? = null

    val serviceIntentReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (mf == null) {
                when {
                    intent?.action?.equals(INTENT_VOLUME) == true -> {
                        val res = intent.getIntExtra("volume", 0)
                        DirectModeKeepAlive.volume = res / 8 + 1
                    }
                    intent?.action?.equals(INTENT_TRANSMIT) == true -> {
                        val start = intent.getBooleanExtra("start", false)
                        if (intent.hasExtra("id")) {
                            val id = intent.getStringExtra("id")
                            if (start) {
                                if (audioThroughPCM) {
                                    recorder = MediaRecorder().apply {
                                        setAudioSource(1998)
                                        setAudioChannels(2)
                                        setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                                        setOutputFile(context?.filesDir?.path + fileName)
                                        setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
                                        try {
                                            prepare()
                                            start()
                                        } catch (e: IOException) {
                                            Log.e(TAG, "prepare() failed ${e.message}")
                                        }
                                    }
                                }
                                Toast.makeText(
                                    serviceContext,
                                    "User $id is speaking",
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                recorder?.apply {
                                    stop()
                                    release()
                                }
                                recorder = null
                                Toast.makeText(
                                    serviceContext,
                                    "User $id stopped speaking",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        } else {
                            if (start) {
                                Log.d(TAG, "ACK BEGIN TRANSMIT")
                                if (micThroughPCM) {
                                    player = MediaPlayer().apply {
                                        try {
                                            setDataSource(context?.filesDir?.path + fileName)
                                            setVolume(0.0f, 1.0f)
                                            prepare()
                                            start()
                                        } catch (e: IOException) {
                                            Log.e(TAG, "prepare() failed ${e.message}")
                                        }
                                    }
                                }
                            } else {
                                Log.d(TAG, "ACK END TRANSMIT")
                                if (DirectModeAPI.micThroughPCM) {
                                    player?.release()
                                    player = null
                                }
                            }
                        }
                    }
                    intent?.action?.equals(INTENT_CHANNEL) == true -> {
                        val res = intent.getIntExtra("channel", 1)
                        DirectModeKeepAlive.channel = res
                    }
                    intent?.action?.equals(INTENT_DIRECT_MODE) == true -> {
                        DirectModeKeepAlive.enabled = intent.getBooleanExtra("start", false)
                    }
                    intent?.action?.equals(INTENT_CHIP_LOCK_UP) == true -> {
                        Toast.makeText(
                            serviceContext,
                            "Notification: There is a chip lock up",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
                return
            }
            Log.i(TAG, "Received intent $intent")
            when {
                intent?.action?.equals(INTENT_VOLUME) == true -> {
                    val volumePicker = mf.rootView?.findViewById<NumberPicker>(R.id.volume_picker)
                    val res = intent.getIntExtra("volume", 0)
                    volumePicker?.value = res / 8 + 1
                    DirectModeKeepAlive.volume = res / 8 + 1
                    Log.d(TAG, "ACK Volume $res")
                }
                intent?.action?.equals(INTENT_TRANSMIT) == true -> {
                    val start = intent.getBooleanExtra("start", false)
                    if (!intent.hasExtra("id")) {
                        val pttButton = mf.rootView?.findViewById<Button>(R.id.ptt_button)
                        pttButton?.isPressed = start
                    }
                }
                intent?.action?.equals(INTENT_CHIP_UPDATE) == true -> {
                    val txt = intent.getStringExtra("msg")
                    Toast.makeText(
                        mf.context,
                        txt,
                        Toast.LENGTH_SHORT).show()
                }
                intent?.action?.equals(INTENT_REGISTER_LISTENER) == true -> {
                    Log.d(TAG, "ACK Registration")
                }
                intent?.action?.equals(INTENT_SET_OWN_ID) == true -> {
                    Log.d(TAG, "ACK SET ID")
                }
                intent?.action?.equals(INTENT_CHANNEL) == true -> {
                    val channelPicker = mf.rootView?.findViewById<NumberPicker>(R.id.channel_picker)
                    val ch = intent.getIntExtra("channel", 1)
                    channelPicker?.value = ch
                    DirectModeKeepAlive.channel = ch
                    micThroughPCM = !intent.getBooleanExtra("mic_in_dm", true)
                    audioThroughPCM = !intent.getBooleanExtra("audio_to_spkr", true)
                    RecordingFragment.rootView?.findViewById<CheckBox>(R.id.pcm_mic)?.isChecked = micThroughPCM
                    RecordingFragment.rootView?.findViewById<CheckBox>(R.id.pcm_audio)?.isChecked = audioThroughPCM
                    Log.d(TAG, "ACK Channel: $ch, AudioPCM: $audioThroughPCM, MicPCM: $micThroughPCM")
                }
                intent?.action?.equals(INTENT_DIRECT_MODE) == true -> {
                    val pttButton = mf.rootView?.findViewById<Button>(R.id.ptt_button)
                    val msgButton = mf.rootView?.findViewById<Button>(R.id.msg_btn)
                    val channelPicker = mf.rootView?.findViewById<NumberPicker>(R.id.channel_picker)
                    val swPower = mf.rootView?.findViewById<Switch>(R.id.enable_switch)
                    if (intent.getBooleanExtra("start", false)) {
                        Log.d(TAG, "DirectMode is enabled")
                        pttButton?.isEnabled = true
                        msgButton?.isEnabled = true
                        channelPicker?.isEnabled = true
                        swPower?.isChecked = true
                        DirectModeKeepAlive.enabled = true
                    } else {
                        Log.d(TAG, "DirectMode is disabled")
                        pttButton?.isEnabled = false
                        msgButton?.isEnabled = false
                        channelPicker?.isEnabled = false
                        swPower?.isChecked = false
                        DirectModeKeepAlive.enabled = false
                    }
                    if (intent.hasExtra("halted")) {
                        if (intent.getBooleanExtra("halted", false)) {
                            Toast.makeText(
                                mf.context,
                                "INFO: Direct Mode is halted because of a phone call!!!",
                                Toast.LENGTH_LONG).show()
                        } else {
                            Toast.makeText(
                                mf.context,
                                "INFO: Direct Mode is resuming after the phone call!!!",
                                Toast.LENGTH_LONG).show()
                        }
                    }
                }
                intent?.action?.equals(INTENT_TEXT_MESSAGE) == true -> {
                    if (intent.hasExtra("id")) {
                        val id = intent.getStringExtra("id").trim()
                        val txt = intent.getStringExtra("msg").trim()
                        val rxText = mf.rootView?.findViewById<EditText>(R.id.rxText)
                        rxText?.text?.append("\n$id: $txt")
                    } else
                        Log.d(TAG, "ACK TEXT MESSAGE")
                }
                intent?.action?.equals(INTENT_CHIP_LOCK_UP) == true -> {
                    Toast.makeText(
                        mf.context,
                        "Notification: There is a chip lock up",
                        Toast.LENGTH_LONG
                    ).show()
                }
                else -> Toast.makeText(
                    mf.context,
                    "Unhandled Intent $intent From Server.",
                    Toast.LENGTH_LONG).show()
            }
        }
    }

    private val handler: Handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            if (mf == null) return
            when {
                msg.what == MSG_SELECT_VOLUME -> {
                    val volumePicker = mf.rootView?.findViewById<NumberPicker>(R.id.volume_picker)
                    volumePicker?.value = msg.arg1 / 8 + 1
                }
                msg.what == MSG_BEGIN_TRANSMIT -> {
                    if (msg.data.isEmpty) {
                        val pttButton = mf.rootView?.findViewById<Button>(R.id.ptt_button)
                        pttButton?.isPressed = true
                        Log.d(TAG, "ACK BEGIN TRANSMIT")
                    } else {
                        val id = msg.data.getString("id")
                        Toast.makeText(
                            mf.context,
                            "User $id is speaking",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
                msg.what == MSG_END_TRANSMIT -> {
                    if (msg.data.isEmpty) {
                        val pttButton = mf.rootView?.findViewById<Button>(R.id.ptt_button)
                        pttButton?.isPressed = false
                        Log.d(TAG, "ACK END TRANSMIT")
                    } else {
                        val id = msg.data.getString("id")
                        Toast.makeText(
                            mf.context,
                            "User $id stopped speaking",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
                msg.what == MSG_UPDATE_NOTIFY -> {
                    val txt = msg.data.getString("msg")
                    Toast.makeText(
                        mf.context,
                        txt,
                        Toast.LENGTH_SHORT).show()
                }
                msg.what == MSG_REGISTER_LISTENER -> {
                    Log.d(TAG, "ACK Registration")
                }
                msg.what == MSG_SET_OWN_ID -> {
                    Log.d(TAG, "ACK SET ID")
                }
                msg.what == MSG_SELECT_CHANNEL -> {
                    val channelPicker = mf.rootView?.findViewById<NumberPicker>(R.id.channel_picker)
                    channelPicker?.value = msg.arg1
                    Log.d(TAG, "ACK Channel ${msg.arg1}")
                }
                msg.what == MSG_POWER_DIRECT_MODE -> {
                    val pttButton = mf.rootView?.findViewById<Button>(R.id.ptt_button)
                    val idButton = mf.rootView?.findViewById<Button>(R.id.id_btn)
                    val msgButton = mf.rootView?.findViewById<Button>(R.id.msg_btn)
                    val channelPicker = mf.rootView?.findViewById<NumberPicker>(R.id.channel_picker)
                    val txEditor = mf.rootView?.findViewById<EditText>(R.id.txText)
                    val swPower = mf.rootView?.findViewById<Switch>(R.id.enable_switch)
                    when {
                        msg.arg1 == 0 -> {
                            txEditor?.isEnabled = false
                            pttButton?.isEnabled = false
                            idButton?.isEnabled = false
                            msgButton?.isEnabled = false
                            channelPicker?.isEnabled = false
                            swPower?.isChecked = false
                        }
                        msg.arg1 == 1 -> {
                            txEditor?.isEnabled = true
                            pttButton?.isEnabled = true
                            idButton?.isEnabled = true
                            msgButton?.isEnabled = true
                            channelPicker?.isEnabled = true
                            swPower?.isChecked = true
                        }
                        msg.arg1 == 2 -> Toast.makeText(
                            mf.context,
                            "INFO: Direct Mode is halted because of a phone call!!!",
                            Toast.LENGTH_LONG).show()
                        else -> Toast.makeText(
                            mf.context,
                            "ERROR: Direct Mode failed to start!!!",
                            Toast.LENGTH_LONG).show()
                    }
                }
                msg.what == MSG_TEXT_MESSAGE -> {
                    if (msg.data.isEmpty) {
                        Log.d(TAG, "ACK TEXT MESSAGE")
                    } else {
                        val bundle = msg.data
                        val id = bundle.getString("id").trim()
                        val txt = bundle.getString("msg").trim()
                        val rxText = mf.rootView?.findViewById<EditText>(R.id.rxText)
                        rxText?.text?.append("\n$id: $txt")
                    }
                }
                msg.what == MSG_AUDIO_TO_SPEAKER -> {
                    Log.d(TAG, "ACK AUDIO_TO_SPEAKER ${msg.arg1}")
                }
                msg.what == MSG_MIC_TO_DIRECTMODE -> {
                    Log.d(TAG, "ACK MIC_TO_DIRECTMODE ${msg.arg1}")
                }
                msg.what == MSG_DIRECT_MODE_LOCK_UP -> {
                    Log.e(TAG, "Warning: There is a chip lock up!")
                    Toast.makeText(
                        mf.context,
                        "There is a chip lock up. To recover please restart direct mode!",
                        Toast.LENGTH_LONG).show()
                }
                else -> Toast.makeText(
                    mf.context,
                    "Unhandled Response ${msg.what} From Server.",
                    Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun sendIntent(msgid: Int, arg1: Int, txt: String?) {
        val intent: Intent
        when (msgid){
            MSG_SELECT_VOLUME -> {
                intent = Intent(INTENT_VOLUME)
                if (arg1 >= 0)
                    intent.putExtra("volume", arg1)
            }
            MSG_BEGIN_TRANSMIT -> {
                intent = Intent(INTENT_TRANSMIT)
                intent.putExtra("start", true)
                Log.d(TAG, "Begin Transmit with $sendID")
                if (!sendID)
                    intent.putExtra("send_id", false)
            }
            MSG_END_TRANSMIT -> {
                intent = Intent(INTENT_TRANSMIT)
                intent.putExtra("start", false)
            }
            MSG_SET_OWN_ID -> {
                intent = Intent(INTENT_SET_OWN_ID)
                intent.putExtra("id", txt)
            }
            MSG_SELECT_CHANNEL -> {
                intent = Intent(INTENT_CHANNEL)
                if (arg1 > 0)
                    intent.putExtra("channel", arg1)
            }
            MSG_MIC_TO_DIRECTMODE -> {
                intent = Intent(INTENT_CHANNEL)
                if (arg1 >= 0)
                    intent.putExtra("mic_in_dm", !micThroughPCM)
            }
            MSG_AUDIO_TO_SPEAKER -> {
                intent = Intent(INTENT_CHANNEL)
                if (arg1 >= 0)
                    intent.putExtra("audio_to_spkr", !audioThroughPCM)
            }
            MSG_POWER_DIRECT_MODE -> {
                intent = Intent(INTENT_DIRECT_MODE)
                if (arg1 >= 0)
                    intent.putExtra("start", arg1==1)
            }
            MSG_TEXT_MESSAGE -> {
                intent = Intent(INTENT_TEXT_MESSAGE)
                intent.putExtra("msg", txt)
            }
            MSG_REGISTER_LISTENER -> {
                intent = Intent(INTENT_REGISTER_LISTENER)
                intent.putExtra("add", arg1==1)
                intent.putExtra("pkg", "com.andromeda.directmodeapp")
                intent.putExtra("cls", "com.andromeda.directmodeapp.MyReceiver")
            }
            MSG_UPDATE_NOTIFY -> {
                Log.e(TAG, "We should never sent this intent, only receive one while mf chip gets updated")
                return
            }
            else -> {
                Log.e(TAG, "Failed to send intent")
                return
            }
        }
        val componentName = ComponentName("com.andromedadigital.andromedadirectmode",
            "com.andromedadigital.andromedadirectmode.IntentReceiver")
        intent.component = componentName
        try {
            when {
                mf != null -> mf.context?.sendBroadcast(intent)
                serviceContext != null -> serviceContext?.sendBroadcast(intent)
                else -> Log.e(TAG, "Cannot find a valid context to send Broadcast")
            }
        } catch (e: Exception) {
            Log.e(TAG, "Failed to send intent")
        }
    }

    fun sendMessageInt(msgid: Int, arg1: Int) {
        if (mf == null || useService) return sendIntent(msgid, arg1, null)
        if (!mBound) return
        // Create and send a message to the service, using a supported 'what' value
        val msg = Message.obtain(null, msgid, arg1, 0)
        try {
            msg.replyTo = Messenger(handler)
            mService?.send(msg)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    fun sendMessageStr(msgid: Int, txt: String) {
        if (mf == null || useService) return sendIntent(msgid, 0, txt)
        if (!mBound) return
        val msg = Message.obtain(null, msgid, 1, 0)
        val bundle = Bundle()
        if (msgid == MSG_SET_OWN_ID)
            bundle.putString("id", txt)
        else
            bundle.putString("msg", txt)
        msg.data = bundle
        msg.replyTo = Messenger(handler)
        try {
            mService?.send(msg)
        } catch (e: RemoteException) {
            Log.d(TAG, "Failed to send message")
        }

    }

    fun registerListener() {
        Log.d(TAG, "Register Client")
        sendMessageInt(MSG_REGISTER_LISTENER, 1)
    }

    fun unregisterListener() {
        Log.d(TAG, "UnRegister Client")
        sendMessageInt(MSG_REGISTER_LISTENER, 0)
    }

    fun enableDirectMode() {
        Log.d(TAG, "enabling direct mode")
        sendMessageInt(MSG_POWER_DIRECT_MODE, 1)
    }

    fun disableDirectMode() {
        Log.d(TAG, "disabling direct mode")
        sendMessageInt(MSG_POWER_DIRECT_MODE, 0)
    }

    fun selectChannel(channel: Int) {
        Log.d(TAG, "selecting channel $channel")
        sendMessageInt(MSG_SELECT_CHANNEL, channel)
    }

    fun selectVolume(volume: Int) {
        Log.d(TAG, "selecting volume $volume")
        sendMessageInt(MSG_SELECT_VOLUME, 8*(volume-1))
    }

    fun beginTransmit() {
        Log.d(TAG, "beginning transmit")
        sendMessageInt(MSG_BEGIN_TRANSMIT, 0)
    }

    fun endTransmit() {
        Log.d(TAG, "ending transmit")
        sendMessageInt(MSG_END_TRANSMIT, 0)
    }

}