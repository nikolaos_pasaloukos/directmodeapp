package com.andromeda.directmodeapp

import android.app.*
import android.content.*
import android.os.Binder
import android.os.IBinder
import android.util.Log
import android.support.v4.content.LocalBroadcastManager

class DirectModeKeepAlive : Service() {
    companion object {
        private const val ONGOING_NOTIFICATION_ID = 1
        private const val TAG = "DirectModeKeepAlive"
        private const val NOTIFICATION_CHANNEL = "andromeda-ptt"
        var running =  false
        var channel = -1
        var volume = -1
        var enabled = false
        var sendID = true
        var micPCM = false
        var audioPCM = false
    }

    private val binder = LocalBinder()
    private var dmAPI: DirectModeAPI = DirectModeAPI(null)
    private val btnReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                getString(R.string.GENAKER_PTT_DOWN),
                getString(R.string.MOTOROLA_PTT_DOWN),
                getString(R.string.ZELLO_PTT_DOWN),
                getString(R.string.ANDROMEDA_PTT_DOWN) -> {
                    Log.d(TAG, "BeginTransmit")
                    dmAPI.beginTransmit()
                }
                getString(R.string.GENAKER_PTT_UP),
                getString(R.string.MOTOROLA_PTT_UP),
                getString(R.string.ZELLO_PTT_UP),
                getString(R.string.ANDROMEDA_PTT_UP) -> {
                    Log.d(TAG, "EndTransmit")
                    dmAPI.endTransmit()
                }
            }
        }
    }

    inner class LocalBinder : Binder() {
        val serverInstance: DirectModeKeepAlive
            get() = this@DirectModeKeepAlive
    }

    override fun onBind(intent: Intent): IBinder {
        Log.d(TAG, "onBind")
        running = true
        return binder
    }

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "onCreate")
        dmAPI.serviceContext = applicationContext
        running = true
        val channel = NotificationChannel(NOTIFICATION_CHANNEL,
            NOTIFICATION_CHANNEL, NotificationManager.IMPORTANCE_LOW)
        channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(channel)

        startForegroundService(DirectModeAPI.getServiceIntent())

        val dmIFilter = IntentFilter()
        //adding some filters
        dmIFilter.addAction(DirectModeAPI.INTENT_SET_OWN_ID)
        dmIFilter.addAction(DirectModeAPI.INTENT_TRANSMIT)
        dmIFilter.addAction(DirectModeAPI.INTENT_DIRECT_MODE)
        dmIFilter.addAction(DirectModeAPI.INTENT_CHANNEL)
        dmIFilter.addAction(DirectModeAPI.INTENT_TEXT_MESSAGE)
        dmIFilter.addAction(DirectModeAPI.INTENT_VOLUME)
        dmIFilter.addAction(DirectModeAPI.INTENT_POWER)
        dmIFilter.addAction(DirectModeAPI.INTENT_CHIP_UPDATE)
        dmIFilter.addAction(DirectModeAPI.INTENT_REGISTER_LISTENER)
        LocalBroadcastManager.getInstance(this).registerReceiver(dmAPI.serviceIntentReceiver, dmIFilter)
        val btnIFilter = IntentFilter()
        btnIFilter.addAction(getString(R.string.GENAKER_PTT_DOWN))
        btnIFilter.addAction(getString(R.string.GENAKER_PTT_UP))
        btnIFilter.addAction(getString(R.string.MOTOROLA_PTT_DOWN))
        btnIFilter.addAction(getString(R.string.MOTOROLA_PTT_UP))
        btnIFilter.addAction(getString(R.string.ZELLO_PTT_DOWN))
        btnIFilter.addAction(getString(R.string.ZELLO_PTT_UP))
        btnIFilter.addAction(getString(R.string.ANDROMEDA_PTT_DOWN))
        btnIFilter.addAction(getString(R.string.ANDROMEDA_PTT_UP))
        LocalBroadcastManager.getInstance(this).registerReceiver(btnReceiver, btnIFilter)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
        running = false
        LocalBroadcastManager.getInstance(this).unregisterReceiver(btnReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(dmAPI.serviceIntentReceiver)
        stopService(DirectModeAPI.getServiceIntent())
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        Log.d(TAG, "onStartCommand")

        val pendingIntent: PendingIntent = Intent(this, DirectMode::class.java).let {
                notificationIntent -> PendingIntent.getActivity(this, 0, notificationIntent, 0)
        }

        val notification = Notification.Builder(this, NOTIFICATION_CHANNEL)
            .setContentTitle(getText(R.string.notification_title))
            .setContentText(getText(R.string.notification_message))
            .setSmallIcon(R.drawable.radio)
            .setContentIntent(pendingIntent)
            .setVisibility(Notification.VISIBILITY_PUBLIC)
            .build()

        startForeground(ONGOING_NOTIFICATION_ID, notification)

        dmAPI.registerListener()
        dmAPI.sendMessageInt(DirectModeAPI.MSG_POWER_DIRECT_MODE, -1)
        dmAPI.sendMessageInt(DirectModeAPI.MSG_SELECT_VOLUME, -1)
        dmAPI.sendMessageInt(DirectModeAPI.MSG_SELECT_CHANNEL, -1)

        return START_STICKY
    }

}
