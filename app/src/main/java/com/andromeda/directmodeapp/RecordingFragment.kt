package com.andromeda.directmodeapp

import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import java.io.IOException
import android.media.AudioAttributes


class RecordingFragment : Fragment(), CompoundButton.OnCheckedChangeListener, View.OnTouchListener {
    companion object {
        var rootView: View? = null
    }
    private val TAG = "DirectModeApp.RecordingFragment"

    private var recorder: MediaRecorder? = null
    private var player: MediaPlayer? = null

    private fun directModeService(context: Context?, start: Boolean) {
        val intent = Intent(context, DirectModeKeepAlive::class.java)
        if (start && !DirectModeKeepAlive.running) {
            DirectModeKeepAlive.running = true
            context?.startForegroundService(intent)
        } else if (!start && DirectModeKeepAlive.running) {
            DirectModeKeepAlive.running = false
            context?.stopService(intent)
        }
    }

    private fun startRecording() {
        recorder = MediaRecorder().apply {
            setAudioSource(1998)
            setAudioChannels(2)
            setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            setOutputFile(context?.filesDir?.path + DirectModeAPI.fileName)
            setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
            try {
                prepare()
                start()
            } catch (e: IOException) {
                Log.e(TAG, "prepare() failed ${e.message}")
            }
        }
    }

    private fun stopRecording() {
        recorder?.apply {
            stop()
            release()
        }
        recorder = null
    }

    private fun startPlaying() {
        player = MediaPlayer().apply {
            try {
                val audioAttributes = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build()
                setAudioAttributes(audioAttributes)
                setVolume(1.0f, 1.0f)
                setDataSource(context?.filesDir?.path + DirectModeAPI.fileName)
                prepare()
                start()
            } catch (e: IOException) {
                Log.e(TAG, "prepare() failed ${e.message}")
            }
        }
    }

    private fun stopPlaying() {
        player?.release()
        player = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        Log.d(TAG, "OnCreateView RecordingFragment")
        rootView = inflater.inflate(R.layout.fragment_recording, container, false)
        val enableSwitch = rootView?.findViewById<Switch>(R.id.switch_service)
        enableSwitch?.setOnCheckedChangeListener(this)
        enableSwitch?.isChecked = DirectModeKeepAlive.running
        val playButton = rootView?.findViewById<Button>(R.id.playBtn)
        playButton?.setOnTouchListener(this)
        playButton?.isEnabled = true
        val recButton = rootView?.findViewById<Button>(R.id.recBtn)
        recButton?.setOnTouchListener(this)
        recButton?.isEnabled = true
        if (DirectModeKeepAlive.running) {
            val micPCM = rootView?.findViewById<CheckBox>(R.id.pcm_mic)
            val audioPCM = rootView?.findViewById<CheckBox>(R.id.pcm_audio)
            val sendId = rootView?.findViewById<CheckBox>(R.id.sendID)
            micPCM?.isChecked = DirectModeKeepAlive.micPCM
            audioPCM?.isChecked = DirectModeKeepAlive.audioPCM
            sendId?.isChecked =  DirectModeKeepAlive.sendID
        }
        return rootView
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        if (isChecked) {
            directModeService(activity?.applicationContext, true)
        } else {
            directModeService(activity?.applicationContext, false)
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        if (event == null || v == null) {
            return false
        }
        v.performClick()
        val playButton = rootView?.findViewById<Button>(R.id.playBtn)
        val recButton = rootView?.findViewById<Button>(R.id.recBtn)
        if (v == playButton) {
            if (event.action == MotionEvent.ACTION_DOWN && v.isEnabled) {
                playButton.isPressed = true
                startPlaying()
            } else if (event.action == MotionEvent.ACTION_UP && v.isEnabled) {
                playButton.isPressed = false
                stopPlaying()
            }
        } else if (v == recButton) {
            if (event.action == MotionEvent.ACTION_DOWN) {
                Log.d(TAG, "RecButton pressed")
                recButton.isPressed = true
                startRecording()
            } else if (event.action == MotionEvent.ACTION_UP && v.isEnabled) {
                Log.d(TAG, "RecButton released")
                recButton.isPressed = false
                stopRecording()
            }
        }
        return true
    }
}